<?php
add_theme_support( 'post-thumbnails' );

function get_page_link_by_slug($page_slug) {
  $category_id = get_cat_ID( $page_slug );
  if ($category_id) :
    return get_category_link( $category_id );
  else :
    return "#";
  endif;
}

//Full image size for the galleries
function wpse_141896_shortcode_atts_gallery( $out )
{
    remove_filter( current_filter(), __FUNCTION__ );
    $out['size'] = 'full';
    return $out;
}

// Numbered Pagination
if ( !function_exists( 'wpex_pagination' ) ) {
  
  function wpex_pagination() {
    
    $prev_arrow = is_rtl() ? '&rarr;' : '&larr;';
    $next_arrow = is_rtl() ? '&larr;' : '&rarr;';
    
    global $wp_query;
    $total = $wp_query->max_num_pages;
    $big = 999999999; // need an unlikely integer
    if( $total > 1 )  {
       if( !$current_page = get_query_var('paged') )
         $current_page = 1;
       if( get_option('permalink_structure') ) {
         $format = 'page/%#%/';
       } else {
         $format = '&paged=%#%';
       }
      echo paginate_links(array(
        'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format'    => $format,
        'current'   => max( 1, get_query_var('paged') ),
        'total'     => $total,
        'mid_size'    => 3,
        'type'      => 'list',
        'prev_text'   => $prev_arrow,
        'next_text'   => $next_arrow,
       ) );
    }
  }
  
}
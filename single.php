<?php
get_header();
add_filter( 'shortcode_atts_gallery', 'wpse_141896_shortcode_atts_gallery' );
$link = get_permalink();
?>
<div class="container single">
	<?php
	if (have_posts()) {
		while (have_posts()) {
			the_post();
	        $link = get_permalink();
    	    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
    	    $post_categories = get_the_category($post->ID);?>
    	    <div class="row">
    	    	<div class="col-md-12">
    	    		<h1 class="single-title"><?php the_title(); ?></h1>
    	    	</div>
    	    </div>
    	    <div class="row">
    	    	<div class="col-md-9">
    	    		<div class="row">
    	    			<div class="col-md-1 cat-title"><?php echo $post_categories[0]->cat_name ?></div>
    	    			<div class="col-md-2"><p>Hace <?php echo $gmt_timestamp = get_post_time('g', true); ?> horas</p></div>
    	    			<div class="col-md-2"><a href="https://twitter.com/share" data-text="<?php the_title(); ?>" data-via="los_amateurs" class="twitter-share-button">Tweet</a></div>
    	    			<div class="col-md-3">
    	    				<div class="fb-like" data-href="<?php echo $link; ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
    	    			</div>
    	    		</div>
    	    	</div>
    	    </div>
    	    <?php
    	    if (in_category('Fotos')) {
    	    	$gallery = get_post_gallery_images($post->ID);
			?>
			<div id="main-carousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<?php
					$n = 0;
					foreach ( $gallery as $image ) {
						?>
						<div class="item <?php echo $n == 0 ? 'active' : ''; ?>">
							<div class="carousel-img" style="background: url(<?php echo $image ?>) no-repeat center center; background-size: cover"></div>
						</div>
						<?php
						$n++;
					}
					wp_reset_postdata();
					?>
				</div>
				<a class="left carousel-control" href="#main-carousel" role="button" data-slide="prev">Prev</a>
				<a class="right carousel-control" href="#main-carousel" role="button" data-slide="next">Next</a>
			</div>
			<div class="row">
				<div class="col-md-9">
					<div class="content"><?php the_content(); ?></div>
					<div class="comments">
						<h4>Comentarios</h4>
					</div>
					<div class="fb-comments" data-href="<?php echo $link?>" data-width="598"></div>
				</div>
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-12">
							<!-- Banner aquí -->
						</div>
					</div>
					<div class="fb-like-box" style="width: 100%; margin-top: 32px;" data-href="https://www.facebook.com/Amateurss" data-width="300" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
				</div>
			</div>
			<?php 
			}
			else {
			?>
			<div class="row">
				<div class="col-md-9">
					<div class="feature-thumb" style="background: url(<?php echo $thumb[0] ?>) no-repeat center center; background-size: cover">Imagén de portada</div>
					<div class="row">
						<div class="col-md-6"></div>
						<div class="col-md-6"></div>
					</div>
					<div class="content"><?php the_content(); ?></div>
					<div class="row">
						<div class="col-md-3">
    	    				<div class="fb-like" data-href="<?php echo $link; ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
    	    			</div>
    	    			<div class="col-md-2"><a href="https://twitter.com/share" data-text="<?php the_title(); ?>" data-via="los_amateurs" class="twitter-share-button">Tweet</a></div>
    	    		</div>
					<div class="comments">
						<h4>Comentarios</h4>
					</div>
					<div class="fb-comments" data-href="<?php echo $link?>" data-width="598"></div>
				</div>
				<div class="col-md-3">
					<div class="fb-like-box" style="width: 100%; margin-top: 32px;" data-href="https://www.facebook.com/Amateurss" data-width="300" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
				</div>
			</div>
			<?php
			}
			?>
    <?php
		}
	}
	?>
</div>
<?php get_footer(); ?>
<?php
get_header();
$conciertos = array(
  'posts_per_page' => 3,
  'orderby' => 'date',
  'category_name' => 'Concierto'
);
$losConciertos = new WP_Query($conciertos);
?>
<div class="container category">
  <div class="row">
    <div class="col-md-7">
    <?php
    if ( have_posts() ) :
    ?>
      <h2 class="main-title"><?php single_cat_title() ?></h2>
      <?php
      while ( have_posts() ) :
        the_post();
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
      ?>
      <div class="row main-list-block">
        <div class="col-md-12 post-title">
          <h3>
            <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
          </h3>
        </div>
        <div class="col-md-6">
          <div class="post-med-img" style="background: url(<?php echo $thumb[0] ?>) no-repeat center center; background-size: cover"></div>
        </div>
        <div class="col-md-6">
          <div class="excerpt-block"><?php the_excerpt(); ?></div>
          <p class="ver-mas"><a href="<?php the_permalink();?>">Ver más</a></p>
        </div>
      </div>
    
    <?php
      endwhile;
    endif;
    wpex_pagination();
    ?>
    </div>
    <div class="col-md-5 side-bar">
      <div class="row">
        <div class="col-md-12">
          <h3 class="nuevas-title">Conciertos</h3>
        </div>
      </div>
      <?php if($losConciertos->have_posts()) : while($losConciertos->have_posts()) : $losConciertos->the_post();
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
      <div class="row conciertos">
        <div class="col-md-4">
          <div class="conciertos-thumb" style="background: url(<?php echo $thumb[0] ?>) no-repeat center center; background-size: cover"></div>
        </div>
        <div class="col-md-8">
          <h3 class="conciertos-title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
          <div class="conciertos-excerpt"><?php the_excerpt(); ?></div>
        </div>
      </div>
      <?php endwhile; endif; ?>
      <div class="row">
        <div class="col-md-12">
          <div class="fb-like-box" style="width: 100%; margin-top: 32px;" data-href="https://www.facebook.com/Amateurss" data-width="300" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
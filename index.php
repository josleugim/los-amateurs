<?php
get_header();
$args = array(
  'posts_per_page' => 5,
  'tag' => 'destacado'
  );
$myposts = get_posts( $args );
?>
<div class="container">
	<div id="main-carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators dots">
      <?php
      $n = 0;
      foreach ( $myposts as $post ) : setup_postdata( $post );
      ?>
      <li data-target="#main-carousel" data-slide-to="<?php echo $n ?>" class="<?php echo $n == 0 ? 'active' : ''; ?>"></li>
      <?php
      $n++;
      endforeach;
      wp_reset_postdata();
      ?>
    </ol>
		
  	<div class="carousel-inner">
      <?php
      $n = 0;
      foreach ( $myposts as $post ) : setup_postdata( $post );
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
      ?>
      <div class="item <?php echo $n == 0 ? 'active' : ''; ?>">
        <div class="carousel-img" style="background: url(<?php echo $thumb[0] ?>) no-repeat center center; background-size: cover">
          <div class="carousel-title">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <?php the_excerpt(); ?>
          </div>
          <div class="carousel-caption"></div>
        </div>
      </div>
      <?php
      $n++;
      endforeach;
      wp_reset_postdata();
      ?>
    </div>
    <a class="left carousel-control" href="#main-carousel" role="button" data-slide="prev">Prev</a>
    <a class="right carousel-control" href="#main-carousel" role="button" data-slide="next">Next</a>

    <ul class="carousel-indicators thumb">
      <?php
      $n = 0;
      foreach ( $myposts as $post ) : setup_postdata( $post );
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
      ?>
      <li data-target="#main-carousel" data-slide-to="<?php echo $n ?>" class="<?php echo $n == 0 ? 'active' : ''; ?>" style="background: url(<?php echo $thumb[0] ?>) no-repeat center center; background-size: cover">
        <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
      </li>
      <?php
      $n++;
      endforeach;
      wp_reset_postdata();
      ?>
    </ul>
	</div>
</div>
<?php
//
$nuevasnuevas = array(
  'posts_per_page' => 5,
  'orderby' => 'date',
  'tag__not_in' => 3
);

$conciertos = array(
  'posts_per_page' => 3,
  'orderby' => 'date',
  'category_name' => 'concierto-agenda'
);

$lasNuevas = new WP_Query($nuevasnuevas);
$losConciertos = new WP_Query($conciertos);
?>
<section class="container">
  <div class="row">
    <div class="col-md-7 nuevas">
      <div class="row">
        <div class="col-md-6">
          <h3 class="nuevas-title">Las nuevas nuevas</h3>
        </div>
      </div>
      <?php if($lasNuevas->have_posts()) : while($lasNuevas->have_posts()) : $lasNuevas->the_post();
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
      $post_categories = get_the_category($post->ID);?>
      <div class="row nuevas-cat">
        <?php foreach( $post_categories  as $cat ) { ?>
        <div class="col-md-2">
          
          <a href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->cat_name; ?></a>
        </div>
        <?php } ?>
        <div class="col-md-3">Hace <?php echo $gmt_timestamp = get_post_time('g', true); ?> horas</div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="nuevas-wrapper">
            <h3 class="nuevas-post-title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
            <div class="nuevas-img" style="background: url(<?php echo $thumb[0] ?>) no-repeat center center; background-size: cover"></div>
            <div class="nuevas-excerpt"><?php the_excerpt(); ?></div>
            <p class="nuevas-mas"><a href="<?php the_permalink();?>">Ver más</a></p>
          </div>
        </div>
      </div>
      <?php endwhile; endif; ?>
    </div>
    <div class="col-md-5 side-bar">
      <div class="row">
        <div class="col-md-12">
          <!-- Banner aquí -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <h3 class="nuevas-title">Conciertos</h3>
        </div>
      </div>
      <?php if($losConciertos->have_posts()) : while($losConciertos->have_posts()) : $losConciertos->the_post();
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
      <div class="row conciertos">
        <div class="col-md-4">
          <div class="conciertos-thumb" style="background: url(<?php echo $thumb[0] ?>) no-repeat center center; background-size: cover"></div>
        </div>
        <div class="col-md-8">
          <h3 class="conciertos-title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
          <div class="conciertos-excerpt"><?php the_excerpt(); ?></div>
        </div>
      </div>
      <?php endwhile; endif; ?>
      <div class="row">
        <div class="col-md-12">
          <div class="fb-like-box" style="width: 100%; margin-top: 32px;" data-href="https://www.facebook.com/Amateurss" data-width="300" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
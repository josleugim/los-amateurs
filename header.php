<!DOCTYPE html>
<!--[if IE 7]>
  <html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
  <html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    <?php // WordPress custom title script
    // is the current page a tag archive page?
    if (function_exists('is_tag') && is_tag()) { 
      // if so, display this custom title
      echo 'Tag : '.$tag.' - '; 
    // or, is the page an archive page?
    } elseif (is_archive()) { 
      // if so, display this custom title
      wp_title(''); echo ' - '; 
    // or, is the page a search page?
    } elseif (is_search()) { 
      // if so, display this custom title
      echo 'Búsqueda: '.wp_specialchars($s).' - '; 
    // or, is the page a single post or a literal page?
    } elseif (!(is_404()) && (is_single()) || (is_page())) { 
      // if so, display this custom title
      wp_title(''); echo ' - '; 
    // or, is the page an error page?
    } elseif (is_404()) {
      // yep, you guessed it
      echo 'No encontrado - '; 
    }
    // finally, display the blog name for all page types
    bloginfo('name');
    ?>
  </title>

  <!-- Bootstrap -->
  <link href="<?php echo get_template_directory_uri() ?>/src/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri() ?>/style.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="<?php echo get_page_link_by_slug('Blog');?>">Blog</a> <span>/</span>
        </li>
        <li><a href="<?php echo get_page_link_by_slug('Agenda');?>">Gigs</a> <span>/</span></li>
        <li><a href="<?php echo get_page_link_by_slug('Videos');?>">Videofeed</a> <span>/</span></li>
        <li><a href="<?php echo get_page_link_by_slug('Fotos');?>">Photo</a></li>
        <li class="instagram"><a href="http://instagram.com/los_amateurs" target="_blank">Instagram</a></li>
        <li class="twitter"><a href="https://twitter.com/los_amateurs" target="_blank">Twitter</a></li>
        <li class="facebook"><a href="https://www.facebook.com/Amateurss" target="_blank">Facebook</a></li>
        <li class="search-field">
          <form>
            <input type="text" name="search">
          </form>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>